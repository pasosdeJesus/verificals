Verificador de un archivo CSV con datos de Luchas Sociales en el formato 
manejado por la línea Movimientos Sociales del CINEP/PPP.

Modo de utilización
ruby ./verifica.rb bd.csv sal.csv

Siendo 
  * bd.csv el archivo con los datos
  * sal.csv el archivo que se generará con errores

